package loop

import (
	"encoding/json"
	"time"

	"github.com/golang/glog"

	"gitlab.com/games/tic_tac_toe/internal/game"
	"gitlab.com/games/tic_tac_toe/pkgs/middleware"
)

// MainLoop for sending back the game changed state to the users
func MainLoop(context *middleware.ContextT) {

	g := game.T{}
	for {
		allActiveGames, err := g.GetActiveGameList(context)
		if err != nil {
			// sleeps for 10 seconds
			time.Sleep(time.Duration(time.Second * 10))
		}
		for _, game := range allActiveGames {
			glog.Info("Sending data for game:", game.ID)
			game.FillGameBoard()
			data, err := json.Marshal(game)
			if err != nil {
				glog.Error("Error in marshalling game data:", err.Error())
				continue
			}
			var c middleware.Client
			c.ClientID = game.Player1ID
			c.GameID = game.ID
			client1, ok := context.Pool.Clients[c.GetUniqueIndetifier()]
			if !ok {
				glog.Error("user ", c.ClientID, " is not connected with socket for game ", game.ID)
			} else {
				client1.WriteChannel <- data
				glog.Info("data send to:", game.Player1ID)
			}

			c.ClientID = *game.Player2ID
			c.GameID = game.ID
			client2, ok := context.Pool.Clients[c.GetUniqueIndetifier()]
			if !ok {
				glog.Error("user ", c.ClientID, " is not connected with socket for game ", game.ID)
			} else {
				client2.WriteChannel <- data
				glog.Info("data send to:", game.Player2ID)
			}
		}
		// sleeps for 10 seconds
		time.Sleep(time.Duration(time.Second * 10))
	}

}
