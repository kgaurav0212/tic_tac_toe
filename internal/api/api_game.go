package api

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"

	"github.com/golang/glog"
	"github.com/gorilla/websocket"
	"gitlab.com/games/tic_tac_toe/internal/game"
	"gitlab.com/games/tic_tac_toe/internal/user"
	"gitlab.com/games/tic_tac_toe/pkgs/middleware"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin:     func(r *http.Request) bool { return true },
}

// ServeWebSocket serves the websocket connection
func ServeWebSocket(context *middleware.ContextT, w http.ResponseWriter, r *http.Request) error {
	gameID, err := strconv.ParseUint(context.URLParams[0], 10, 64)
	if err != nil {
		glog.Error("Invalid game id", err.Error())
		return err
	}
	g := game.T{}
	g.ID = gameID
	err = g.Get(context)
	if err != nil {
		glog.Error("error getting game details", err.Error())
		return err
	}
	if g.Player1ID != context.User.ID && g.Player2ID == nil && *g.Player2ID != context.User.ID {
		glog.Error("Invalid user for the game")
		return errors.New("Invalid user for the game")
	}
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		glog.Error("error in upgrading to socket connection", err.Error())
		return err
	}
	client := &middleware.Client{
		ClientID:     context.User.ID,
		GameID:       gameID,
		Conn:         conn,
		WriteChannel: make(chan []byte),
		Pool:         context.Pool,
	}
	context.Pool.Register <- client
	readChannel := make(chan []byte)
	go client.Write()
	go client.Read(readChannel)
	for data := range readChannel {
		glog.Info("Received data from socket", string(data))
		markedData := game.MarkT{}
		err := json.Unmarshal(data, &markedData)
		if err != nil {
			glog.Error("invalid data sends through socket, ", err.Error())
			continue
		}
		markedData.GameID = gameID
		updatedGame, err := markedData.Update(context)
		if err != nil {
			glog.Error("UserID: ", context.User.ID, "error in updating game board, ", err.Error())
			continue
		}
		data, _ := json.Marshal(updatedGame)
		c1 := middleware.Client{GameID: updatedGame.ID, ClientID: updatedGame.Player1ID}
		c2 := middleware.Client{GameID: updatedGame.ID, ClientID: *updatedGame.Player2ID}
		context.Pool.Clients[c1.GetUniqueIndetifier()].WriteChannel <- data
		context.Pool.Clients[c2.GetUniqueIndetifier()].WriteChannel <- data
		if updatedGame.PlayerWon != 0 {
			break
		}
	}
	return nil
}

// SignUp to register in the system
func SignUp(context *middleware.ContextT, r *http.Request) (interface{}, int) {
	u := user.T{}
	if err := json.NewDecoder(r.Body).Decode(&u); err != nil {
		glog.Error("Error in input data pasring", err.Error())
		return err.Error(), http.StatusBadRequest
	}
	if err := u.Validate(); err != nil {
		glog.Error("error in validating input data", err.Error())
		return err.Error(), http.StatusBadRequest
	}
	if err := u.Add(context); err != nil {
		glog.Error("error in inserting the data in database", err.Error())
		return err.Error(), http.StatusInternalServerError
	}
	glog.Info("User entered data is:", u)
	jwt, err := u.GenerateToken(context)
	if err != nil {
		glog.Error("error in generating token of the user", err.Error())
		return err.Error(), http.StatusInternalServerError
	}
	glog.Info("User token data is:", jwt)
	return jwt, http.StatusOK
}

// SignIn to validate user data and sign in to system
func SignIn(context *middleware.ContextT, r *http.Request) (interface{}, int) {
	u := user.T{}
	if err := json.NewDecoder(r.Body).Decode(&u); err != nil {
		glog.Error("Error in input data pasring", err.Error())
		return err.Error(), http.StatusBadRequest
	}
	storedUser := user.T{}
	storedUser.EmailID = u.EmailID
	if err := storedUser.Get(context); err != nil {
		glog.Error("Error in getting the specified user from database", err.Error())
		return err.Error(), http.StatusInternalServerError
	}
	glog.Info("User get data is:", storedUser)
	if err := u.IsCorrectPassword(storedUser.Password); err != nil {
		glog.Error("Incorrect password provided", err.Error())
		return err.Error(), http.StatusUnauthorized
	}
	jwt, err := storedUser.GenerateToken(context)
	if err != nil {
		glog.Error("error in generating token of the user", err.Error())
		return err.Error(), http.StatusInternalServerError
	}
	glog.Info("User jwt data is:", jwt)
	return jwt, http.StatusOK
}

// CreateGame creates a game room with one player joined as craeting user
func CreateGame(context *middleware.ContextT, r *http.Request) (interface{}, int) {
	g := game.T{}
	if err := g.Add(context); err != nil {
		glog.Error("error in inserting the data in database", err.Error())
		return err.Error(), http.StatusInternalServerError
	}
	glog.Info("Game created with data:", g)
	return g, http.StatusOK
}

// FindAllVacantGames to find all games with one vacant player
func FindAllVacantGames(context *middleware.ContextT, r *http.Request) (interface{}, int) {
	// default offset and limit
	offset := 0
	limit := 10
	if offsets, ok := context.GetParams["offset"]; ok && len(offsets) > 0 {
		offset, _ = strconv.Atoi(offsets[0])

	}
	if limits, ok := context.GetParams["limit"]; ok && len(limits) > 0 {
		limit, _ = strconv.Atoi(limits[0])
	}
	g := game.T{}
	games, err := g.GetVacantGameList(context, offset, limit)
	if err != nil {
		glog.Error("error in getting data from database", err.Error())
		return err.Error(), http.StatusInternalServerError
	}
	glog.Info("Returned games are:", games)
	return games, http.StatusOK
}

// JoinGame to join the given game
func JoinGame(context *middleware.ContextT, r *http.Request) (interface{}, int) {
	g := game.T{}
	if len(context.URLParams) < 1 {
		glog.Error("gameID is not in url")
		return nil, http.StatusBadRequest
	}
	gameID, err := strconv.ParseUint(context.URLParams[0], 10, 64)
	if err != nil {
		glog.Error("Non integer gameID passed", err.Error())
		return nil, http.StatusBadRequest
	}
	g.ID = gameID
	if err := g.Join(context); err != nil {
		glog.Error("Error in joinig game_id:", gameID, " player_id:", context.User.ID)
		return err.Error(), http.StatusInternalServerError
	}
	return g, http.StatusOK
}
