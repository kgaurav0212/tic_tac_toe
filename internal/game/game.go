package game

import (
	"errors"
	"math/rand"
	"time"

	"gitlab.com/games/tic_tac_toe/pkgs/middleware"
	"gitlab.com/games/tic_tac_toe/pkgs/models"
)

// T struct for defining custom methods over Game model
type T struct {
	models.Game
}

// MarkT indicated marked postion in game
type MarkT struct {
	GameID       uint64
	MarkedRow    uint
	MarkedColumn uint
}

// Add to create new game
func (g *T) Add(context *middleware.ContextT) error {
	//set id to zero for using increment id
	g.ID = 0
	g.Player1ID = context.User.ID
	g.FillGameBoard()
	if err := g.FillGameBoardJSON(); err != nil {
		return err
	}
	return context.DB.Create(&g.Game).Error
}

// Join the game as a second player
func (g *T) Join(context *middleware.ContextT) error {
	db := context.DB
	if g.ID == 0 {
		return errors.New("IDs should be positive")
	}
	rand.Seed(time.Now().UnixNano())
	updatedGame := models.Game{CurrentPlayer: models.PlayerT(rand.Intn(2) + 1), Player2ID: &context.User.ID}
	where := models.Game{Model: models.Model{ID: g.ID}, Player2ID: nil}
	rowsAffected := db.Model(&models.Game{}).Not("player1_id = ?", context.User.ID).Where(where).Updates(updatedGame).RowsAffected
	if rowsAffected == 0 {
		return errors.New("Unable to join the game")
	}
	// get the updated game
	if err := g.Get(context); err != nil {
		return err
	}
	return nil
}

// GetVacantGameList returns the paginated form list of games that are available to join
func (g *T) GetVacantGameList(context *middleware.ContextT, offset, limit int) ([]models.Game, error) {
	var games []models.Game
	db := context.DB
	var where models.Game
	where.Player2ID = nil
	if err := db.Offset(offset).Limit(limit).Order("updated_at desc").Find(&games, where).Error; err != nil {
		return nil, err
	}
	return games, nil
}

// GetActiveGameList returns all the games that are currently in play state
func (g *T) GetActiveGameList(context *middleware.ContextT) ([]models.Game, error) {
	var games []models.Game
	db := context.DB
	if err := db.Order("updated_at desc").Find(&games, "player2_id IS NOT NULL AND player_won = 0").Error; err != nil {
		return games, err
	}
	return games, nil
}

// Get fills the game object
func (g *T) Get(context *middleware.ContextT) error {
	db := context.DB
	where := models.Game{Model: models.Model{ID: g.ID}}
	if err := db.Find(&g.Game, where).Error; err != nil {
		return errors.New("Error in getting game data")
	}
	g.FillGameBoard()
	return nil
}

func (g *T) updateGameBoard(m *MarkT) error {
	// update game board
	if g.GameBoard[m.MarkedRow][m.MarkedColumn] != 0 {
		return errors.New("Already marked position")
	}
	g.GameBoard[m.MarkedRow][m.MarkedColumn] = g.CurrentPlayer
	g.FillGameBoardJSON()
	return nil
}

func (g *T) updateNextPlayer(currentPlayerID uint64) error {
	// update next Player
	var nextPlayer models.PlayerT
	if g.Player1ID == currentPlayerID && g.CurrentPlayer == models.Player1 {
		// If this was first player chance
		nextPlayer = models.Player2
	} else if *g.Player2ID == currentPlayerID && g.CurrentPlayer == models.Player2 {
		// If this was second player chance
		nextPlayer = models.Player1
	} else {
		// If Wrong player take chance
		return errors.New("Not your turn for playing game")
	}
	g.CurrentPlayer = nextPlayer
	return nil
}

func (g *T) updatePlayerWon() {
	// update player Won

	// row conditions
	for row := 0; row < len(g.GameBoard); row++ {
		if g.GameBoard[row][0] != 0 && g.GameBoard[row][0] == g.GameBoard[row][1] && g.GameBoard[row][1] == g.GameBoard[row][2] {
			g.PlayerWon = g.GameBoard[row][0]
			return
		}
	}

	// column conditions
	for column := 0; column < len(g.GameBoard[0]); column++ {
		if g.GameBoard[0][column] != 0 && g.GameBoard[0][column] == g.GameBoard[1][column] && g.GameBoard[1][column] == g.GameBoard[2][column] {
			g.PlayerWon = g.GameBoard[0][column]
			return
		}
	}

	// diagonal1 condition
	if g.GameBoard[0][0] != 0 && g.GameBoard[0][0] == g.GameBoard[1][1] && g.GameBoard[1][1] == g.GameBoard[2][2] {
		g.PlayerWon = g.GameBoard[0][0]
		return
	}
	// diagonal2 condition
	if g.GameBoard[0][2] != 0 && g.GameBoard[0][2] == g.GameBoard[1][1] && g.GameBoard[1][1] == g.GameBoard[2][0] {
		g.PlayerWon = g.GameBoard[0][2]
		return
	}

	gameDraw := true
	for i := 0; i < len(g.GameBoard); i++ {
		for j := 0; j < len(g.GameBoard[i]); j++ {
			if g.GameBoard[i][j] == 0 {
				gameDraw = false
			}
		}
	}
	if gameDraw {
		g.PlayerWon = models.PlayerUnknown
	}
}

// Update game based on the new board position
func (m *MarkT) Update(context *middleware.ContextT) (T, error) {
	db := context.DB
	updatedGame := T{}
	storedGame := T{}
	where := models.Game{Model: models.Model{ID: m.GameID}}
	if err := db.Find(&storedGame.Game, where).Error; err != nil {
		return storedGame, errors.New("Error in getting game data")
	}
	storedGame.FillGameBoard()

	// If game is over
	if storedGame.PlayerWon != 0 {
		return updatedGame, errors.New("Game already finished")
	}

	// if marked position is overflowed
	if len(storedGame.GameBoard) == 0 || (m.MarkedRow >= uint(len(storedGame.GameBoard)) || m.MarkedColumn >= uint(len(storedGame.GameBoard[0]))) {
		return updatedGame, errors.New("Invalid Position for marking")
	}

	// update the marked position
	if err := storedGame.updateGameBoard(m); err != nil {
		return updatedGame, err
	}

	// update the next player turn
	if err := storedGame.updateNextPlayer(context.User.ID); err != nil {
		return updatedGame, err
	}

	// update winner
	storedGame.updatePlayerWon()

	err := db.Model(&models.Game{}).Where(where).Updates(storedGame.Game).Error
	if err != nil {
		return updatedGame, errors.New("Error while joining game")
	}
	// get the updated game
	updatedGame.ID = m.GameID
	if err := updatedGame.Get(context); err != nil {
		return updatedGame, err
	}

	return updatedGame, nil
}
