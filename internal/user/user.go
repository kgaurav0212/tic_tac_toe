package user

import (
	"time"

	"gitlab.com/games/tic_tac_toe/pkgs/middleware"
	"gitlab.com/games/tic_tac_toe/pkgs/models"
)

const (
	//LongLived token time
	LongLived = time.Duration(7 * 24 * time.Hour)
)

// T struct for defining custom methods over User model
type T struct {
	models.User
}

// Validate the input data is in correct range
func (u *T) Validate() error {
	return nil
}

// Add to create new user
func (u *T) Add(context *middleware.ContextT) error {
	//set id to zero for using increment id
	u.ID = 0
	if err := u.HashPassword(); err != nil {
		return err
	}
	return context.DB.Create(&u.User).Error
}

// Get to get the specified user
func (u *T) Get(context *middleware.ContextT) error {
	//set id to zero for using increment id
	db := context.DB
	where := models.User{EmailID: u.EmailID}
	if err := db.Find(&u.User, where).Error; err != nil {
		return err
	}
	return nil
}

// GenerateToken creates a new token for user
func (u *T) GenerateToken(context *middleware.ContextT) (middleware.JwtT, error) {
	jwtToken := middleware.JwtT{SecretKey: context.SecretKey}
	if err := jwtToken.CreateToken(middleware.UserContextT{ID: u.ID}); err != nil {
		return jwtToken, err
	}
	// clears secret key before passing back to user
	jwtToken.SecretKey = ""
	return jwtToken, nil
}
