Requirement:
mysql 8

Update the config.json find with the mysql config

Keep the game in <go_path>/src/gitlab.com/games/<tic_tac_toe>

cd <tic_tac_toe>/cmd/router/
Download all dependency:
go get

Run game:
go run main.go

SignUp Request:
POST /tic*tac_toe/sign_up HTTP/1.1
Host: localhost:8000
Content-Type: application/json
User-Agent: PostmanRuntime/7.15.0
Accept: */\_
Cache-Control: no-cache
Postman-Token: 372470fc-2aa9-400d-a531-00cb2ab474ab,83137f56-b82f-41f2-a828-5434fc5696f9
Host: localhost:8000
accept-encoding: gzip, deflate
content-length: 88
Connection: keep-alive
cache-control: no-cache

{
"Name": "Gaurav Kumar",
"EmailID": "kgaurav0212@gmail.com",
"Password": "getmega"
}

SignIn Request:
POST /tic*tac_toe/sign_in HTTP/1.1
Host: localhost:8000
Content-Type: application/json
User-Agent: PostmanRuntime/7.15.0
Accept: */\_
Cache-Control: no-cache
Postman-Token: 2f222beb-ed61-4670-875f-26a4ffad9e33,1fdb8b39-a09e-41c3-b9cc-0e6b73b3d8ce
Host: localhost:8000
accept-encoding: gzip, deflate
content-length: 88
Connection: keep-alive
cache-control: no-cache

{
"Name": "Gaurav Kumar",
"EmailID": "kgaurav0212@gmail.com",
"Password": "getmega"
}

Create Game Request:
POST /tic*tac_toe/create_game HTTP/1.1
Host: localhost:8000
Content-Type: application/json
Authorization: <token>
User-Agent: PostmanRuntime/7.15.0
Accept: */\_
Cache-Control: no-cache
Postman-Token: 1b89ddba-80b3-41bb-bcc1-2cbbcccbef1d,cddb7ce9-a3aa-4876-97f5-92ddfb2f881f
Host: localhost:8000
accept-encoding: gzip, deflate
content-length: 4
Connection: keep-alive
cache-control: no-cache

{

}

Find All Vacant Game:
GET /tic*tac_toe/find_all_vacant_games HTTP/1.1
Host: localhost:8000
Content-Type: application/json
Authorization: <token>
User-Agent: PostmanRuntime/7.15.0
Accept: */\_
Cache-Control: no-cache
Postman-Token: 95582edd-0e27-46aa-89fc-5badf426b524,ffc74f28-ae40-4f69-94b3-c8dff35aaa3a
Host: localhost:8000
accept-encoding: gzip, deflate
content-length: 88
Connection: keep-alive
cache-control: no-cache

{
"Name": "Gaurav Kumar",
"EmailID": "kgaurav0212@gmail.com",
"Password": "getmega"
}

Join Game:
POST /tic*tac_toe/join_game/3 HTTP/1.1
Host: localhost:8000
Content-Type: application/json
Authorization: <token>
User-Agent: PostmanRuntime/7.15.0
Accept: */\_
Cache-Control: no-cache
Postman-Token: 83bb2143-471d-4253-b798-6d46c8857dc0,21bed735-c288-419b-bcfb-0c1252bbc8a1
Host: localhost:8000
accept-encoding: gzip, deflate
content-length: 4
Connection: keep-alive
cache-control: no-cache

{

}

Connect to game with socket:
var conn = new WebSocket('ws://localhost:8000/tic_tac_toe/connect/<game_id>/<user_token>');

Making a move
conn.send('{"MarkedRow":2, "MarkedColumn":0}')
