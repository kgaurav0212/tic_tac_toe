package main

import (
	"github.com/golang/glog"
	"github.com/zenazn/goji"
	"gitlab.com/games/tic_tac_toe/internal/api"
	"gitlab.com/games/tic_tac_toe/internal/loop"
	"gitlab.com/games/tic_tac_toe/pkgs/middleware"
)

func main() {
	glog.Info("starting game server")
	var context middleware.ContextT
	context.Build()
	defer context.Close()
	// start game loop which send data of all active games
	go loop.MainLoop(&context)

	goji.Post("/tic_tac_toe/sign_up", middleware.AppHandlerT{ContextT: &context, CheckToken: false, H: api.SignUp})
	goji.Post("/tic_tac_toe/sign_in", middleware.AppHandlerT{ContextT: &context, CheckToken: false, H: api.SignIn})
	goji.Post("/tic_tac_toe/create_game", middleware.AppHandlerT{ContextT: &context, CheckToken: true, H: api.CreateGame})
	goji.Post("/tic_tac_toe/join_game/*", middleware.AppHandlerT{ContextT: &context, CheckToken: true, H: api.JoinGame})
	goji.Get("/tic_tac_toe/find_all_vacant_games", middleware.AppHandlerT{ContextT: &context, CheckToken: true, H: api.FindAllVacantGames})
	goji.Get("/tic_tac_toe/connect/*", middleware.WebSocketHandlerT{ContextT: &context, H: api.ServeWebSocket})

	goji.Serve()
}
