package middleware

import (
	"net/http"
	"strconv"
	"strings"

	"github.com/golang/glog"
	"github.com/gorilla/websocket"
	"github.com/zenazn/goji/web"
)

// Pool for socket connection management
type Pool struct {
	Context    *ContextT
	Clients    map[string]*Client
	Register   chan *Client
	Unregister chan *Client
}

// Client for websocket client
type Client struct {
	ClientID     uint64
	GameID       uint64
	WriteChannel chan []byte
	Conn         *websocket.Conn
	Pool         *Pool
}

//Read on websocket
func (c *Client) Read(readchannel chan<- []byte) {
	defer func() {
		c.Pool.Unregister <- c
		c.Conn.Close()
	}()
	for {
		_, message, err := c.Conn.ReadMessage()
		if err != nil {
			close(readchannel)
			glog.Error("error in reading data from socket", err.Error())
			return
		}
		readchannel <- message
	}
}

// Write to websocket
func (c *Client) Write() {
	defer func() {
		c.Pool.Unregister <- c
		c.Conn.Close()
	}()
	for message := range c.WriteChannel {
		err := c.Conn.WriteMessage(websocket.TextMessage, message)
		if err != nil {
			glog.Error("error in writing data to socket", err.Error())
		}
	}
}

// GetUniqueIndetifier return per game basis unique connection
func (c *Client) GetUniqueIndetifier() string {
	return strconv.FormatUint(c.ClientID, 10) + "_" + strconv.FormatUint(c.GameID, 10)
}

// WebSocketHandlerT contains the basic context and function type for websockets connection
type WebSocketHandlerT struct {
	*ContextT
	CheckToken bool
	H          func(*ContextT, http.ResponseWriter, *http.Request) error
}

// ServeHTTPC for websocket connection
func (ah WebSocketHandlerT) ServeHTTPC(c web.C, w http.ResponseWriter, r *http.Request) {
	AllowCrossOriginOptions(w, r)
	ctx := ContextT{AppContextT: ah.ContextT.AppContextT}
	urlParams := strings.Split(r.URL.Path, "/")
	if len(urlParams) < 5 {
		w.WriteHeader(http.StatusNetworkAuthenticationRequired)
		return
	}
	jwtToken := JwtT{Token: urlParams[4], SecretKey: ctx.SecretKey}
	var err error
	ctx.User, err = jwtToken.ParseToken()
	if err != nil {
		w.WriteHeader(http.StatusNetworkAuthenticationRequired)
		return
	}
	ctx.URLParams = urlParams[3:]
	err = ah.H(&ctx, w, r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	}
}
