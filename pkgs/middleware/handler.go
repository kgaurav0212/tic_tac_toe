package middleware

import (
	"encoding/json"
	"net/http"
	"strings"

	"github.com/zenazn/goji/web"
	"github.com/zenazn/goji/web/middleware"
)

// AppHandlerT contains the basic context and function type
type AppHandlerT struct {
	*ContextT
	CheckToken bool
	H          func(*ContextT, *http.Request) (interface{}, int)
}

type outputT struct {
	RequestID string
	Data      interface{}
}

// AllowCrossOriginOptions allows access of the APIs from different origins
func AllowCrossOriginOptions(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, DELETE, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type, Authorization")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
}

// ServeHTTPC gets the request and act as a middleware
func (ah AppHandlerT) ServeHTTPC(c web.C, w http.ResponseWriter, r *http.Request) {
	AllowCrossOriginOptions(w, r)
	requestID := middleware.GetReqID(c)
	ctx := ContextT{AppContextT: ah.ContextT.AppContextT}
	ctx.GetParams = r.URL.Query()
	// removes first blank string and store rest in URLParams
	ctx.URLParams = strings.Split(r.URL.Path, "/")[1:]
	ctx.RequestID = requestID
	//extract user information from token
	if ah.CheckToken {
		jwtToken := JwtT{Token: r.Header.Get("authorization"), SecretKey: ctx.SecretKey}
		var err error
		ctx.User, err = jwtToken.ParseToken()
		if err != nil {
			//invalid or expired token
			w.WriteHeader(http.StatusNetworkAuthenticationRequired)
			json.NewEncoder(w).Encode(outputT{RequestID: requestID, Data: err.Error()})
			return
		}
	}
	// remove initial two service name and request name
	ctx.URLParams = ctx.URLParams[2:]
	// select {
	// case <-ctx.Done():
	// 	fmt.Println("Time to return")
	// }
	data, status := ah.H(&ctx, r)
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(outputT{RequestID: requestID, Data: data})
}
