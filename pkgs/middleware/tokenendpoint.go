package middleware

import (
	"errors"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/mitchellh/mapstructure"
)

const (
	//LongLived token time
	LongLived = time.Duration(7 * 24 * time.Hour)
)

// JwtT for token and secret storage
type JwtT struct {
	Token     string
	SecretKey string `json:",omitempty"`
}

// UserContextT stores the user ID and privilages
type UserContextT struct {
	ID uint64
}

// CreateToken takes userDetails and provide token for authentication
func (jwtToken *JwtT) CreateToken(user UserContextT) error {
	var mapClaims jwt.MapClaims

	mapClaims = jwt.MapClaims{
		"id":  user.ID,
		"exp": time.Now().Add(LongLived).Unix(),
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, mapClaims)
	tokenString, err := token.SignedString([]byte(jwtToken.SecretKey))
	if err != nil {
		return err
	}
	jwtToken.Token = "BEARER " + tokenString
	return nil
}

// ParseToken to get user information from token
func (jwtToken *JwtT) ParseToken() (UserContextT, error) {
	var user UserContextT
	splittedToken := strings.Split(jwtToken.Token, " ")
	if len(splittedToken) < 2 {
		return user, errors.New("Invalid token")
	}
	_, tokenString := splittedToken[0], splittedToken[1]
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, errors.New("Signing method not compatible")
		}
		return []byte(jwtToken.SecretKey), nil
	})
	if err != nil {
		return user, err
	}
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		mapstructure.Decode(claims, &user)
	} else {
		return user, errors.New("Invalid token")
	}
	if !token.Valid {
		return user, errors.New("Token Expired")
	}
	return user, nil
}
