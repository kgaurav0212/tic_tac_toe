package middleware

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/url"
	"os"
	"path/filepath"

	"github.com/golang/glog"
	"gitlab.com/games/tic_tac_toe/pkgs/adapter"
)

// AppContextT stores connection pointers
type AppContextT struct {
	Pool      *Pool
	DB        adapter.DatabaseT
	SecretKey string
}

// ContextT contains full context
type ContextT struct {
	AppContextT
	context   context.Context
	RequestID string
	User      UserContextT
	URLParams []string
	GetParams url.Values
}

// newPool creates a new manager object
func newPool() *Pool {
	return &Pool{
		Clients:    make(map[string]*Client),
		Register:   make(chan *Client),
		Unregister: make(chan *Client),
	}
}

// Start the websocket server pool register/unregister thread
func (pool *Pool) Start() {
	for {
		select {
		case client := <-pool.Register:
			pool.Clients[client.GetUniqueIndetifier()] = client
			glog.Info("Added new connection!")
		case client := <-pool.Unregister:
			if _, ok := pool.Clients[client.GetUniqueIndetifier()]; ok {
				delete(pool.Clients, client.GetUniqueIndetifier())
				glog.Info("A connection has been terminated!")
			}
		}
	}
}

// Build constructor sets up parameters
func (context *ContextT) Build() {
	glog.Info("Building context")
	absPath, err := filepath.Abs("../../config.json")
	if err != nil {
		panic(err)
	}
	jsonFile, err := os.Open(absPath)
	if err != nil {
		panic(err)
	}
	defer jsonFile.Close()
	byteValue, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		panic(err)
	}
	var config map[string]string
	if err := json.Unmarshal(byteValue, &config); err != nil {
		panic(err)
	}
	context.SecretKey = config["secret"]
	context.DB.EndPoint = config["DBEndPoint"]
	context.DB.Name = config["DBName"]
	context.DB.CreateConnection()
	context.DB.CreateDatabase()
	context.Pool = newPool()
	go context.Pool.Start()
	glog.Info("Building context completed")
}

// Close closes all actice connections
func (context *ContextT) Close() {
	err := context.DB.Close()
	if err != nil {
		panic(err)
	}
}
