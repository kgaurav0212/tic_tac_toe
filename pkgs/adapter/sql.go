package adapter

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/games/tic_tac_toe/pkgs/models"

	//msql driver for gorm
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// DatabaseT has embedded sql.DB to create custom methods over mysql
type DatabaseT struct {
	*gorm.DB
	EndPoint string
	Name     string
}

//CreateConnection returns an database pointer from which we can access database
func (db *DatabaseT) CreateConnection() {
	var err error
	db.DB, err = gorm.Open("mysql", db.EndPoint+db.Name+"?parseTime=true")
	if err != nil {
		panic(err)
	}
}

// CreateDatabase cretes all tables
func (db *DatabaseT) CreateDatabase() {
	db.AutoMigrate(&models.User{})
	db.Model(&models.User{}).AddUniqueIndex("idx_user_email", "email_id")
	db.AutoMigrate(&models.Game{})
	db.Model(&models.Game{}).AddForeignKey("player1_id", "users(id)", "RESTRICT", "NO ACTION")
	db.Model(&models.Game{}).AddForeignKey("player2_id", "users(id)", "RESTRICT", "NO ACTION")
}
