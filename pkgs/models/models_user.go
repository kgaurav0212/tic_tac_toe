package models

import (
	"golang.org/x/crypto/bcrypt"
)

//User for storing user related information
type User struct {
	Model
	Name     string `gorm:"not null"`
	EmailID  string `gorm:"not null" json:",omitempty"`
	Password string `gorm:"not null" json:",omitempty"`
}

// HashPassword inorder to not save direct password string
func (u *User) HashPassword() error {
	hash, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.MinCost)
	if err != nil {
		return err
	}
	u.Password = string(hash)
	return nil
}

// IsCorrectPassword checkts whether the given password matches the hashed password
func (u *User) IsCorrectPassword(hashedPassword string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(u.Password))

}
