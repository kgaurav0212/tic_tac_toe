package models

// PlayerT are the possible player position
type PlayerT uint8

const (
	//Player1 number represntation
	Player1 PlayerT = iota + 1
	//Player2 number represntation
	Player2
	//PlayerUnknown draw winner player
	PlayerUnknown
)

// Game stores all the games that are played on the server
type Game struct {
	Model
	GameBoardJSON JSON          `gorm:"type:json;not null"`
	GameBoard     [3][3]PlayerT `gorm:"-"`
	Player1ID     uint64        `gorm:"not null" json:",omitempty"`
	Player2ID     *uint64       `json:",omitempty"`
	CurrentPlayer PlayerT       `gorm:"not null;default:0" json:",omitempty"`
	PlayerWon     PlayerT       `gorm:"not null;default:0" json:",omitempty"`
}

// FillGameBoard fills array from json
func (g *Game) FillGameBoard() {
	err := g.GameBoardJSON.JSONToStruct(&g.GameBoard)
	if err != nil {
		g.GameBoard = [3][3]PlayerT{{0, 0, 0}, {0, 0, 0}, {0, 0, 0}}
	}
}

// FillGameBoardJSON fills json from GameBoard
func (g *Game) FillGameBoardJSON() error {
	return g.GameBoardJSON.StructToJSON(g.GameBoard)
}
