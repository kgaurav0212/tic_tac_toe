package models

import (
	"encoding/json"
	"time"
)

//Model Base Model's definition
type Model struct {
	ID        uint64    `json:",omitempty"`
	CreatedAt time.Time `gorm:"type:datetime;default:NOW();not null;index" json:",omitempty"`
	UpdatedAt time.Time `gorm:"type:datetime;default:NOW();not null;index" json:",omitempty"`
}

// JSON type for using json features
type JSON string

//JSONToStruct method is to get json value in structure
func (j *JSON) JSONToStruct(v interface{}) error {
	return json.Unmarshal([]byte(*j), v)
}

//StructToJSON from the interface to json
func (j *JSON) StructToJSON(v interface{}) error {
	value, err := json.Marshal(v)
	if err != nil {
		return err
	}
	*j = JSON(value)
	return nil
}
